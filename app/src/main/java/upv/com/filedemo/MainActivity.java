package upv.com.filedemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class MainActivity extends AppCompatActivity {
    Button save,load;
    EditText message, message2, message3;
    String Message,Message2,Message3;
    int data_block=100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        save = (Button)findViewById(R.id.SAVE);
        load = (Button)findViewById(R.id.Load);
        message = (EditText)findViewById(R.id.msg);
        message2 = (EditText)findViewById(R.id.msg2);
        message3 = (EditText)findViewById(R.id.msg3);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                Message = message.getText().toString();
                Message2 = message2.getText().toString();
                Message3 = message3.getText().toString();
                try{
                    FileOutputStream fou = openFileOutput("text.txt",MODE_WORLD_READABLE);
                    OutputStreamWriter osw= new OutputStreamWriter(fou);
                    try{
                        osw.write(Message);
                        osw.write(Message2);
                        osw.write(Message3);
                        osw.flush();
                        osw.close();
                        Toast.makeText(getBaseContext(),"Dato guardado", Toast.LENGTH_LONG).show();
                    }catch(IOException e){
                        e.printStackTrace();
                    }
                } catch(FileNotFoundException e){
                    e.printStackTrace();
                }
            }
        });

        load.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                try{
                    FileInputStream fis = openFileInput("text.txt");
                    InputStreamReader isr = new InputStreamReader(fis);
                    char[] data = new char[data_block];
                    String final_data="";
                    int size;
                    try{
                        while((size=isr.read(data))>0){
                            String read_data = String.valueOf(data,0,size);
                            final_data += read_data;
                            data=new char[data_block];
                        }
                        Toast.makeText(getBaseContext(),"Message:"+final_data, Toast.LENGTH_LONG).show();
                    }catch (IOException e){
                        e.printStackTrace();
                    }
                }catch (FileNotFoundException e){
                    e.printStackTrace();
                }
            }
        });
    }
}
